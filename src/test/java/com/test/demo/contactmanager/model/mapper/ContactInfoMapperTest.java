package com.test.demo.contactmanager.model.mapper;

import com.test.demo.contactmanager.model.entities.ContactInfoEntity;
import com.test.demo.contactmanager.model.entities.AddressEntity;
import com.test.demo.contactmanager.model.vo.ContactInfoVO;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;

/**
 * Created by fadanro on 5/9/18.
 */
public class ContactInfoMapperTest {

  @Test
  public void toContactInfoVO_should_transform_ContactInfoEntity_into_ContactInfoVO() {

    AddressEntity addressEntity = AddressEntity.builder()
      .street("221 B BAKER STREET")
      .city("LONDON")
      .state("LONDON_ST")
      .zipCode("99999-99")
      .country("UK")
      .build();

    ContactInfoEntity contactInfoEntity = ContactInfoEntity.builder()
      .id(1L)
      .name("SHERLOCK")
      .company("SCOTLAND YARD")
      .profileImage("DETECTIVE")
      .email("sherlock_holmes@ukpolice.com")
      .birthDate("1887-04-29")
      .workPhoneNumber("999-983-7895")
      .personalPhoneNumber("209-983-7895")
      .address(addressEntity)
      .build();

    ContactInfoVO contactInfoVO = ContactInfoMapper.toContactInfoVO(contactInfoEntity);

    assertThat(contactInfoVO, is(notNullValue()));
    assertThat(contactInfoVO.getId(), equalTo(1L));
    assertThat(contactInfoVO.getName(), equalTo("SHERLOCK"));
    assertThat(contactInfoVO.getCompany(), equalTo("SCOTLAND YARD"));
    assertThat(contactInfoVO.getProfileImage(), equalTo("DETECTIVE"));
    assertThat(contactInfoVO.getEmail(), equalTo("sherlock_holmes@ukpolice.com"));
    assertThat(contactInfoVO.getBirthDate(), equalTo("1887-04-29"));
    assertThat(contactInfoVO.getWorkPhoneNumber(), equalTo("999-983-7895"));
    assertThat(contactInfoVO.getPersonalPhoneNumber(), equalTo("209-983-7895"));

    assertThat(contactInfoVO.getStreet(), equalTo("221 B BAKER STREET"));
    assertThat(contactInfoVO.getCity(), equalTo("LONDON"));
    assertThat(contactInfoVO.getState(), equalTo("LONDON_ST"));
    assertThat(contactInfoVO.getZipCode(), equalTo("99999-99"));
    assertThat(contactInfoVO.getCountry(), equalTo("UK"));

  }

  @Test
  public void toContactInfoEntity_should_transform_ContactInfoVO_into_ContactInfoEntity() {

    ContactInfoVO contactInfoVO = ContactInfoVO.builder()
      .id(1L)
      .name("SHERLOCK")
      .company("SCOTLAND YARD")
      .profileImage("DETECTIVE")
      .email("sherlock_holmes@ukpolice.com")
      .birthDate("1887-04-29")
      .workPhoneNumber("999-983-7895")
      .personalPhoneNumber("209-983-7895")

      .street("221 B BAKER STREET")
      .city("LONDON")
      .state("LONDON_ST")
      .zipCode("99999-99")
      .country("UK")
      .build();

    ContactInfoEntity contactInfoEntity = ContactInfoMapper.toContactInfoEntity(contactInfoVO);

    assertThat(contactInfoEntity, is(notNullValue()));
    assertThat(contactInfoEntity.getName(), equalTo("SHERLOCK"));
    assertThat(contactInfoEntity.getCompany(), equalTo("SCOTLAND YARD"));
    assertThat(contactInfoEntity.getProfileImage(), equalTo("DETECTIVE"));
    assertThat(contactInfoEntity.getEmail(), equalTo("sherlock_holmes@ukpolice.com"));
    assertThat(contactInfoEntity.getBirthDate(), equalTo("1887-04-29"));
    assertThat(contactInfoEntity.getWorkPhoneNumber(), equalTo("999-983-7895"));
    assertThat(contactInfoEntity.getPersonalPhoneNumber(), equalTo("209-983-7895"));

    assertThat(contactInfoEntity.getAddress().getStreet(), equalTo("221 B BAKER STREET"));
    assertThat(contactInfoEntity.getAddress().getCity(), equalTo("LONDON"));
    assertThat(contactInfoEntity.getAddress().getState(), equalTo("LONDON_ST"));
    assertThat(contactInfoEntity.getAddress().getZipCode(), equalTo("99999-99"));
    assertThat(contactInfoEntity.getAddress().getCountry(), equalTo("UK"));

  }

}