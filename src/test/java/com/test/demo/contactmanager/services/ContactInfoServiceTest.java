package com.test.demo.contactmanager.services;

import com.test.demo.contactmanager.Application;
import com.test.demo.contactmanager.model.vo.ContactInfoVO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by fadanro on 5/9/18.
 */
@RunWith(SpringRunner.class)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@SpringBootTest(classes = Application.class)
public class ContactInfoServiceTest {

  @Autowired
  private ContactInfoService service;

  private ContactInfoVO getDummyContactInfo() {

    ContactInfoVO contactInfoVO = ContactInfoVO.builder()
      .name("MARK")
      .company("WRITERS INC")
      .profileImage("WRITER")
      .email("mark_twain@writersinc.com")
      .birthDate("1835-11-30")
      .personalPhoneNumber("123 465 897")

      .street("400 HANNIBAL AVENUE")
      .city("Florida")
      .state("Missouri")
      .zipCode("123490")
      .country("USA")
      .build();

    return contactInfoVO;
  }

  @Test
  public void addContactInfo_should_create_a_new_contact_info_record() {
    ContactInfoVO contactInfoVO = service.addContactInfo(getDummyContactInfo());

    ContactInfoVO persistedContactInfo = service.getContactInfo(contactInfoVO.getId());

    assertThat(contactInfoVO, equalTo(persistedContactInfo));
  }

  @Test
  public void updateContactInfo_should_update_existing_contact_info() {
    ContactInfoVO contactInfoVO = service.addContactInfo(getDummyContactInfo());

    contactInfoVO.setCompany("WRITERS UNITED");
    contactInfoVO.setBirthDate("1835-10-15");

    ContactInfoVO persistedContactInfo = service.updateContactInfo(contactInfoVO);

    assertThat(persistedContactInfo.getCompany(), equalTo("WRITERS UNITED"));
    assertThat(persistedContactInfo.getBirthDate(), equalTo("1835-10-15"));
  }

  @Test
  public void deleteContactInfo_should_delete_existing_contact() {
    ContactInfoVO contactInfoVO = service.addContactInfo(getDummyContactInfo());

    service.deleteContactInfo(contactInfoVO.getId());

    ContactInfoVO contactInfoAfterDelete = service.getContactInfo(contactInfoVO.getId());

    assertThat(contactInfoAfterDelete, is(nullValue()));
  }

  @Test
  public void getContactInfoByLocation_should_return_all_contacts_that_belong_to_that_location(){
    String location = "LONDON";

    List<ContactInfoVO> contactInfoList = service.getContactInfoByLocation(location);
    assertThat(contactInfoList.size(), equalTo(2));
  }

}