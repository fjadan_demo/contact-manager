package com.test.demo.contactmanager.repository;

import com.test.demo.contactmanager.model.entities.ContactInfoEntity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by fadanro on 5/9/18.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ContactInfoRepositoryTest {

  @Autowired
  private ContactInfoRepository repository;

  @Test
  public void findById_should_return_a_valid_record() throws Exception {
    ContactInfoEntity contactInfoEntity = repository.findById(1L);

    assertThat(contactInfoEntity, is(notNullValue()));
    assertThat(contactInfoEntity.getName(), equalTo("SHERLOCK"));
    assertThat(contactInfoEntity.getCompany(), equalTo("SCOTLAND YARD"));

    assertThat(contactInfoEntity.getAddress().getStreet(), equalTo("221 B BAKER STREET"));
    assertThat(contactInfoEntity.getAddress().getCity(), equalTo("LONDON"));
  }

  @Test
  public void findByEmail_should_return_a_valid_record() throws Exception {

    String email = "john@ukpolice.com";

    ContactInfoEntity contactInfoEntity = repository.findByEmail(email);

    assertThat(contactInfoEntity, is(notNullValue()));
    assertThat(contactInfoEntity.getName(), equalTo("JOHN"));
    assertThat(contactInfoEntity.getCompany(), equalTo("SCOTLAND YARD"));

    assertThat(contactInfoEntity.getAddress().getStreet(), equalTo("123 MAIN STREET"));
    assertThat(contactInfoEntity.getAddress().getCity(), equalTo("LONDON"));
  }

  @Test
  public void findByPhoneNumber_should_return_a_valid_record() throws Exception {

    String phoneNumber = "123 178 354";

    ContactInfoEntity contactInfoEntity = repository.findByPhoneNumber(phoneNumber);

    assertThat(contactInfoEntity, is(notNullValue()));
    assertThat(contactInfoEntity.getName(), equalTo("JOHN"));
    assertThat(contactInfoEntity.getCompany(), equalTo("SCOTLAND YARD"));

    assertThat(contactInfoEntity.getAddress().getStreet(), equalTo("123 MAIN STREET"));
    assertThat(contactInfoEntity.getAddress().getCity(), equalTo("LONDON"));
  }

  @Test
  public void findByEmailAndPhoneNumber_should_return_a_valid_record() throws Exception {

    String email = "mary@university.com";
    String phoneNumber = "567 178 354";

    ContactInfoEntity contactInfoEntity = repository.findByEmailAndPhoneNumber(email, phoneNumber);

    assertThat(contactInfoEntity, is(notNullValue()));
    assertThat(contactInfoEntity.getName(), equalTo("MARY"));
    assertThat(contactInfoEntity.getCompany(), equalTo("UNIVERSITY"));

    assertThat(contactInfoEntity.getAddress().getStreet(), equalTo("400 COMPUTER AVENUE"));
    assertThat(contactInfoEntity.getAddress().getCity(), equalTo("BOSTON"));
  }

  @Test
  public void findByLocation_should_return_all_contacts_that_belong_to_that_location() throws Exception {

    String location = "LONDON";


    List<ContactInfoEntity> contactInfoEntityList = repository.findByLocation(location);

    assertThat(contactInfoEntityList, is(notNullValue()));
    assertThat(contactInfoEntityList.size(), equalTo(2));

    assertThat(contactInfoEntityList.get(0).getName(), equalTo("SHERLOCK"));
    assertThat(contactInfoEntityList.get(0).getCompany(), equalTo("SCOTLAND YARD"));

    assertThat(contactInfoEntityList.get(0).getAddress().getStreet(), equalTo("221 B BAKER STREET"));
    assertThat(contactInfoEntityList.get(0).getAddress().getCity(), equalTo("LONDON"));

    assertThat(contactInfoEntityList.get(1).getName(), equalTo("JOHN"));
    assertThat(contactInfoEntityList.get(1).getCompany(), equalTo("SCOTLAND YARD"));

    assertThat(contactInfoEntityList.get(1).getAddress().getStreet(), equalTo("123 MAIN STREET"));
    assertThat(contactInfoEntityList.get(1).getAddress().getCity(), equalTo("LONDON"));
  }

}