package com.test.demo.contactmanager.rest;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.test.demo.contactmanager.model.vo.ContactInfoVO;
import com.test.demo.contactmanager.services.ContactInfoService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.file.Files;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by fadanro on 5/9/18.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest(ContactInfoSpecification.class)
public class ContactInfoControllerTest {

  @Autowired
  protected WebApplicationContext wac;

  protected MockMvc mockMvc;

  @MockBean
  protected ContactInfoService contactInfoService;

  @MockBean
  private PagedResourcesAssembler<ContactInfoVO> assembler;


  @Before
  public void setup() throws Exception {

    mockMvc = webAppContextSetup(wac)
      .alwaysDo(MockMvcResultHandlers.print())
      .build();
  }

  @Test
  public void testValidCreateContactInfoRequest() throws Exception {

    given(this.contactInfoService.addContactInfo(any(ContactInfoVO.class)))
      .willReturn(ContactInfoVO.builder()
        .id(1L)
        .name("MARK")
        .company("WRITERS INC")
        .profileImage("WRITER")
        .email("mark_twain@writersinc.com")
        .birthDate("1835-11-30")
        .personalPhoneNumber("123 465 897")

        .street("400 HANNIBAL AVENUE")
        .city("Florida")
        .state("Missouri")
        .zipCode("123490")
        .country("USA")
        .build());

    ClassLoader classLoader = getClass().getClassLoader();

    File file = new File(classLoader.getResource("newContactInfoRequest.json").getFile());

    String content = new String(Files.readAllBytes(file.toPath()));

    this.mockMvc.perform(MockMvcRequestBuilders.post("/contact")
      .content(content)
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isCreated());
  }

  @Test
  public void testHateoasLinks() throws Exception {

    given(this.contactInfoService.addContactInfo(any(ContactInfoVO.class)))
      .willReturn(ContactInfoVO.builder()
        .id(1L)
        .name("MARK")
        .company("WRITERS INC")
        .profileImage("WRITER")
        .email("mark_twain@writersinc.com")
        .birthDate("1835-11-30")
        .personalPhoneNumber("123 465 897")

        .street("400 HANNIBAL AVENUE")
        .city("Florida")
        .state("Missouri")
        .country("USA")
        .build());

    ClassLoader classLoader = getClass().getClassLoader();

    File file = new File(classLoader.getResource("newContactInfoRequest.json").getFile());

    String content = new String(Files.readAllBytes(file.toPath()));

    MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post("/contact")
      .content(content)
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isCreated())
      .andReturn();

    Gson gson = new Gson();

    assertThat(gson.fromJson(mvcResult.getResponse().getContentAsString(), JsonElement.class)
      .getAsJsonObject().get("_links").getAsJsonObject().get("self").getAsJsonObject().get("href")
      .getAsString(), equalTo("http://localhost/contact/1"));

  }

  @Test
  public void testBadRequest() throws Exception {

    this.mockMvc.perform(MockMvcRequestBuilders.post("/contact")
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void testValidGetContactInfoRequest() throws Exception {

    given(this.contactInfoService.getContactInfo(any(Long.class)))
      .willReturn(ContactInfoVO.builder()
        .id(1L)
        .name("MARK")
        .company("WRITERS INC")
        .profileImage("WRITER")
        .email("mark_twain@writersinc.com")
        .birthDate("1835-11-30")
        .personalPhoneNumber("123 465 897")

        .street("400 HANNIBAL AVENUE")
        .city("Florida")
        .state("Missouri")
        .zipCode("123490")
        .country("USA")
        .build());

    this.mockMvc.perform(MockMvcRequestBuilders.get("/contact/1")
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk());
  }

  @Test
  public void testGetContactInfoRequestRecordNotFound() throws Exception {

    given(this.contactInfoService.getContactInfo(any(Long.class)))
      .willReturn(null);

    this.mockMvc.perform(MockMvcRequestBuilders.get("/contact/100")
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isNotFound());
  }

}