package com.test.demo.contactmanager.repository;

import com.test.demo.contactmanager.model.entities.ContactInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by franciscoadan on 07/05/18.
 */
public interface ContactInfoRepository extends JpaRepository<ContactInfoEntity, Long> {

  ContactInfoEntity findById(long contactInfoId);

  ContactInfoEntity findByEmail(String email);

  @Query("select cie from ContactInfoEntity cie  where cie.personalPhoneNumber = :phNumber or cie.workPhoneNumber = :phNumber")
  ContactInfoEntity findByPhoneNumber(@Param("phNumber") String phNumber);

  @Query("select cie from ContactInfoEntity cie where cie.email = :email and (cie.personalPhoneNumber = :phNumber or cie.workPhoneNumber = :phNumber)")
  ContactInfoEntity findByEmailAndPhoneNumber(@Param("email") String email, @Param("phNumber") String phNumber);

  @Query("select cie from ContactInfoEntity cie  where cie.address.state = :location or cie.address.city = :location")
  List<ContactInfoEntity> findByLocation(@Param("location") String location);

}
