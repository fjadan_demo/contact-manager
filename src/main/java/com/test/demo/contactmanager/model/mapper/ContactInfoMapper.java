package com.test.demo.contactmanager.model.mapper;

import com.test.demo.contactmanager.model.entities.ContactInfoEntity;
import com.test.demo.contactmanager.model.vo.ContactInfoVO;
import com.test.demo.contactmanager.model.entities.AddressEntity;

/**
 * Created by franciscoadan on 07/05/18.
 */
public class ContactInfoMapper {

  public static ContactInfoVO toContactInfoVO(ContactInfoEntity contactInfoEntity) {

    AddressEntity addressEntity = contactInfoEntity.getAddress();

    ContactInfoVO contactInfoVO = ContactInfoVO.builder()
      .id(contactInfoEntity.getId())
      .name(contactInfoEntity.getName())
      .company(contactInfoEntity.getCompany())
      .profileImage(contactInfoEntity.getProfileImage())
      .email(contactInfoEntity.getEmail())
      .birthDate(contactInfoEntity.getBirthDate())
      .workPhoneNumber(contactInfoEntity.getWorkPhoneNumber())
      .personalPhoneNumber(contactInfoEntity.getPersonalPhoneNumber())

      .street(addressEntity.getStreet())
      .city(addressEntity.getCity())
      .state(addressEntity.getState())
      .zipCode(addressEntity.getZipCode())
      .country(addressEntity.getCountry())
      .build();

    return contactInfoVO;
  }

  public static ContactInfoEntity toContactInfoEntity(ContactInfoVO contactInfoVO) {

    AddressEntity addressEntity = AddressEntity.builder()
      .street(contactInfoVO.getStreet())
      .city(contactInfoVO.getCity())
      .state(contactInfoVO.getState())
      .zipCode(contactInfoVO.getZipCode())
      .country(contactInfoVO.getCountry())
      .build();

    ContactInfoEntity contactInfoEntity = ContactInfoEntity.builder()
      .name(contactInfoVO.getName())
      .company(contactInfoVO.getCompany())
      .profileImage(contactInfoVO.getProfileImage())
      .email(contactInfoVO.getEmail())
      .birthDate(contactInfoVO.getBirthDate())
      .workPhoneNumber(contactInfoVO.getWorkPhoneNumber())
      .personalPhoneNumber(contactInfoVO.getPersonalPhoneNumber())
      .address(addressEntity)
      .build();

    return contactInfoEntity;
  }
}
