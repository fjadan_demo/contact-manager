package com.test.demo.contactmanager.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by franciscoadan on 07/05/18.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class ContactInfoVO {

    private long id;

    @NotNull(message = "name should not ne null")
    private String name;

    private String company;

    private String profileImage;

    private String email;

    @NotNull(message = "birthDate should not ne null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private String birthDate;

    private String workPhoneNumber;

    @NotNull(message = "personalPhoneNumber should not ne null")
    private String personalPhoneNumber;

    @NotNull(message = "street should not ne null")
    private String street;

    @NotNull(message = "city should not ne null")
    private String city;

    @NotNull(message = "state should not ne null")
    private String state;

    @NotNull(message = "zipCode should not ne null")
    private String zipCode;

    @NotNull(message = "country should not ne null")
    private String country;

    @Builder
    public ContactInfoVO(long id,
                         String name,
                         String company,
                         String profileImage,
                         String email,
                         String birthDate,
                         String workPhoneNumber,
                         String personalPhoneNumber,
                         String street,
                         String city,
                         String state,
                         String zipCode,
                         String country
    ) {

        this.id = id;
        this.name = name;
        this.company = company;
        this.profileImage = profileImage;
        this.email = email;
        this.birthDate = birthDate;
        this.workPhoneNumber = workPhoneNumber;
        this.personalPhoneNumber = personalPhoneNumber;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.country = country;
    }

}
