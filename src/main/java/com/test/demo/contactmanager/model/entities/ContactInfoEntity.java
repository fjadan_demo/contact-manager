package com.test.demo.contactmanager.model.entities;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * Created by franciscoadan on 07/05/18.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "CONTACT_INFO")
@Proxy(lazy=false)
public class ContactInfoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "COMPANY")
    private String company;

    @Column(name = "PROFILE_IMAGE")
    private String profileImage;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "BIRTHDATE")
    private String birthDate;

    @Column(name = "WORK_PN")
    private String workPhoneNumber;

    @Column(name = "PERSONAL_PN")
    private String personalPhoneNumber;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID")
    private AddressEntity address;

    @Builder
    public ContactInfoEntity(Long id,
                             String name,
                             String company,
                             String profileImage,
                             String email,
                             String birthDate,
                             String workPhoneNumber,
                             String personalPhoneNumber,
                             AddressEntity address) {

        this.id = id;
        this.name = name;
        this.company = company;
        this.profileImage = profileImage;
        this.email = email;
        this.birthDate = birthDate;
        this.workPhoneNumber = workPhoneNumber;
        this.personalPhoneNumber = personalPhoneNumber;
        this.address = address;
    }

}
