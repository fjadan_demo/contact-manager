package com.test.demo.contactmanager.services;

import com.test.demo.contactmanager.model.entities.ContactInfoEntity;
import com.test.demo.contactmanager.model.mapper.ContactInfoMapper;
import com.test.demo.contactmanager.model.vo.ContactInfoVO;
import com.test.demo.contactmanager.repository.ContactInfoRepository;

import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by franciscoadan on 07/05/18.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ContactInfoService {

  private ContactInfoRepository repository;

  @Autowired
  @Builder
  public ContactInfoService(ContactInfoRepository repository) {
    this.repository = repository;
  }

  public ContactInfoVO addContactInfo(ContactInfoVO contactInfoVO) {
    ContactInfoEntity contactInfoEntity = ContactInfoMapper.toContactInfoEntity(contactInfoVO);

    repository.saveAndFlush(contactInfoEntity);

    contactInfoVO.setId(contactInfoEntity.getId());

    return contactInfoVO;
  }

  public ContactInfoVO updateContactInfo(ContactInfoVO contactInfoVO) {

    ContactInfoEntity existingContactInfoEntity = repository.findById(contactInfoVO.getId());

    if (existingContactInfoEntity != null) {

      ContactInfoEntity contactInfoEntity = ContactInfoMapper.toContactInfoEntity(contactInfoVO);
      contactInfoEntity.setId(contactInfoVO.getId());

      repository.saveAndFlush(contactInfoEntity);

      return contactInfoVO;
    }

    return null;
  }

  public ContactInfoVO getContactInfo(long contactInfoId) {

    ContactInfoVO contactInfoVO = null;

    ContactInfoEntity contactInfoEntity = repository.findById(contactInfoId);

    if (contactInfoEntity != null) {
      contactInfoVO = ContactInfoMapper.toContactInfoVO(contactInfoEntity);
    }

    return contactInfoVO;
  }

  public ContactInfoVO deleteContactInfo(long contactInfoId) {

    ContactInfoVO contactInfoVO = null;

    ContactInfoEntity contactInfoEntity = repository.findById(contactInfoId);

    if (contactInfoEntity != null) {
      contactInfoVO = ContactInfoMapper.toContactInfoVO(contactInfoEntity);

      repository.deleteById(contactInfoId);
    }

    return contactInfoVO;
  }

  public ContactInfoVO getContactInfoByAttributes(String email, String phoneNumber) {

    ContactInfoEntity contactInfoEntity = null;

    if (!StringUtils.isEmpty(email) && !StringUtils.isEmpty(phoneNumber)) {
      contactInfoEntity = repository.findByEmailAndPhoneNumber(email, phoneNumber);

    } else if (!StringUtils.isEmpty(email)) {
      contactInfoEntity = repository.findByEmail(email);

    } else if (!StringUtils.isEmpty(phoneNumber)) {
      contactInfoEntity = repository.findByEmail(email);
    }

    if (contactInfoEntity != null) {
      return ContactInfoMapper.toContactInfoVO(contactInfoEntity);
    }

    return null;
  }

  public List<ContactInfoVO> getContactInfoByLocation(String location) {

    List<ContactInfoEntity> contactInfoEntities = repository.findByLocation(location);

    List<ContactInfoVO> contactInfoVOS = contactInfoEntities.stream()
      .map(contactInfo -> ContactInfoMapper.toContactInfoVO(contactInfo))
      .collect(Collectors.toList());

    return contactInfoVOS;
  }

}
