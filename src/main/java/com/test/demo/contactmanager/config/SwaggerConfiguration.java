package com.test.demo.contactmanager.config;

import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.core.DefaultRelProvider;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

/**
 * Created by fadanro on 5/8/18.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

  private static final String SPRING_HATEOAS_OBJECT_MAPPER = "_halObjectMapper";

  @Autowired
  TypeResolver typeResolver;

  @Autowired
  @Qualifier(SPRING_HATEOAS_OBJECT_MAPPER)
  private ObjectMapper springHateoasObjectMapper;

  @Value("${contact.service.version}")
  private String serviceVersion;

  @Bean
  public Docket fooContactManagerApi() {
    return new Docket(DocumentationType.SWAGGER_2)
      .useDefaultResponseMessages(false)
      .apiInfo(apiInfo())
      .select()
      .paths(regex("/contact.*"))
      .build()
      .alternateTypeRules(
        newRule(
          typeResolver.resolve(Collection.class, WildcardType.class),
          typeResolver.resolve(ResourcesList.class, WildcardType.class)
        ),
        newRule(
          typeResolver.resolve(List.class, Link.class),
          typeResolver.resolve(HashMap.class, String.class, typeResolver.resolve(HashMap.class, String.class, String.class))
        )
      )
      .select()
      .paths(regex("/contact.*"))
      .build()
      .forCodeGeneration(true);
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
      .title("Contact Info Manager")
      .version(serviceVersion)
      .description("Service Manages Contact Information")
      .build();
  }

  @Bean
  public RelProvider relProvider() {
    return new DefaultRelProvider() {
      @Override
      public String getCollectionResourceRelFor(Class<?> type) {
        return "items";
      }
    };
  }

  @Primary
  @Bean
  @Order(value = Ordered.HIGHEST_PRECEDENCE)
  @DependsOn(SPRING_HATEOAS_OBJECT_MAPPER)
  public ObjectMapper objectMapper() {
    return springHateoasObjectMapper;
  }

  public class ResourcesList<T> {
    public List<T> getItems() {
      return null;
    }
  }

}
