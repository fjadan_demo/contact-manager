package com.test.demo.contactmanager.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by fadanro on 5/8/18.
 */
@Configuration
@EnableAutoConfiguration
public class WebApplicationConfiguration {

  @Bean
  public WebMvcConfigurerAdapter forwardRootToSwagger() {

    return new WebMvcConfigurerAdapter() {

      @Override
      public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/swagger-ui.html");
      }

    };
  }

}
