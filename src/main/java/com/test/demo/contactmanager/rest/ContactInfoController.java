package com.test.demo.contactmanager.rest;

import com.test.demo.contactmanager.model.vo.ContactInfoVO;
import com.test.demo.contactmanager.services.ContactInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by franciscoadan on 07/05/18.
 */
@RestController
@RequestMapping("/contact")
public class ContactInfoController implements ContactInfoSpecification {

  @Autowired
  private ContactInfoService contactInfoService;

  @Autowired
  private PagedResourcesAssembler<ContactInfoVO> assembler;

  @ResponseStatus(value = HttpStatus.CREATED)
  @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json;charset=UTF-8", consumes = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> createContactInfo(@RequestBody @Valid ContactInfoVO contactInfoVO) {

    ContactInfoVO persistedContactInfo = contactInfoService.addContactInfo(contactInfoVO);

    if (persistedContactInfo.getId() > 0) {

      return ResponseEntity.status(HttpStatus.CREATED)
        .body(new Resource<>(persistedContactInfo,
          linkTo(methodOn(ContactInfoController.class)
            .getContactInfo(persistedContactInfo.getId())).withSelfRel()
        ));

    } else {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(null);
    }
  }

  @ResponseStatus(value = HttpStatus.OK)
  @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> updateContactInfo(@RequestBody @Valid ContactInfoVO contactInfoVO) {

    ContactInfoVO persistedContactInfo = contactInfoService.updateContactInfo(contactInfoVO);

    if (persistedContactInfo.getId() > 0) {

      return ResponseEntity.status(HttpStatus.OK)
        .body(new Resource<>(persistedContactInfo,
          linkTo(methodOn(ContactInfoController.class)
            .getContactInfo(persistedContactInfo.getId())).withSelfRel()
        ));

    } else {

      return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(null);
    }

  }

  @ResponseStatus(value = HttpStatus.OK)
  @RequestMapping(value = "/{contactInfoId}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> getContactInfo(@PathVariable(name = "contactInfoId", required = true) long contactInfoId) {

    ContactInfoVO persistedContactInfo = contactInfoService.getContactInfo(contactInfoId);

    if (persistedContactInfo != null) {

      return ResponseEntity.status(HttpStatus.OK)
        .body(new Resource<>(persistedContactInfo,
          linkTo(methodOn(ContactInfoController.class)
            .getContactInfo(persistedContactInfo.getId())).withSelfRel()
        ));

    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(null);
    }

  }

  @ResponseStatus(value = HttpStatus.OK)
  @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> getContactInfo(
    @RequestParam(name = "email", required = false) String email,
    @RequestParam(name = "phoneNumber", required = false) String phoneNumber) {

    ContactInfoVO persistedContactInfo = contactInfoService.getContactInfoByAttributes(email, phoneNumber);

    if (persistedContactInfo != null) {

      return ResponseEntity.status(HttpStatus.OK)
        .body(new Resource<>(persistedContactInfo,
          linkTo(methodOn(ContactInfoController.class)
            .getContactInfo(persistedContactInfo.getId())).withSelfRel()
        ));

    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(null);
    }

  }

  @ResponseStatus(value = HttpStatus.OK)
  @RequestMapping(value = "/location/{location}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PagedResources<Resource<ContactInfoVO>>> getContactInfoByLocation(
    @PathVariable(name = "location", required = true) String location,
    @RequestParam(name = "page", required = false) Integer page,
    @RequestParam(name = "pageSize", required = false) Integer pageSize) {

    List<ContactInfoVO> contactList = contactInfoService.getContactInfoByLocation(location);

    if (contactList != null) {

      Long actualRecordCount = Long.valueOf(contactList.size());
      page = page == null ? 1 : page;
      pageSize = pageSize == null ? 50 : pageSize;

      Integer suggestedPageSize = actualRecordCount.intValue() < pageSize ? Integer.valueOf(actualRecordCount.intValue()) : pageSize;
      Integer finalPageSize = suggestedPageSize == 0 ? Integer.valueOf(1) : suggestedPageSize;

      PageRequest pageRequest = new PageRequest(page, finalPageSize, new Sort(Sort.Direction.ASC, "name"));

      Page<ContactInfoVO> contactRecordEntityPage = new PageImpl<ContactInfoVO>(contactList, pageRequest, actualRecordCount);

      PagedResources<Resource<ContactInfoVO>> resourcePagedResources = null;

      resourcePagedResources = assembler.toResource(contactRecordEntityPage);

      for (Resource<ContactInfoVO> contactResource : resourcePagedResources.getContent()) {
        contactResource.add(linkTo(methodOn(ContactInfoController.class)
          .getContactInfo(contactResource.getContent().getId())).withSelfRel());
      }

      return ResponseEntity.status(HttpStatus.OK)
        .body(resourcePagedResources);

    } else {

      return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(null);
    }

  }

  @ResponseStatus(value = HttpStatus.OK)
  @RequestMapping(value = "/{contactInfoId}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> deleteContactInfo(
    @PathVariable(name = "contactInfoId", required = true) long contactInfoId) {

    ContactInfoVO removedContactInfo = contactInfoService.deleteContactInfo(contactInfoId);

    if (removedContactInfo != null) {

      return ResponseEntity.status(HttpStatus.OK)
        .body(new Resource<>(removedContactInfo));

    } else {

      return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(null);
    }

  }

}
