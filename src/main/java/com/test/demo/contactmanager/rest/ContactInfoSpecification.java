package com.test.demo.contactmanager.rest;

import com.test.demo.contactmanager.model.vo.ContactInfoVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

/**
 * Created by fadanro on 5/8/18.
 */
@Api(tags = {"contact"}, description = "Contact Manager Endpoint")
public interface ContactInfoSpecification {

  @ResponseStatus(value = HttpStatus.CREATED)
  @ApiOperation(value = "To create a contact information")
  @ApiResponses(value = {
    @ApiResponse(code = 201, message = "Created",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Success Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 400, message = "Bad Request",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Request Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 415, message = "Unsupported Media Type",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Authorization Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 500, message = "Internal Server Error",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      })})
  @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> createContactInfo(
    @RequestBody @Valid ContactInfoVO contactInfoVO);


  @ResponseStatus(value = HttpStatus.OK)
  @ApiOperation(value = "To update an existing contact information")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Bad Request",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Request Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 415, message = "Unsupported Media Type",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Authorization Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 500, message = "Internal Server Error",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      })})
  @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> updateContactInfo(
    @RequestBody @Valid ContactInfoVO contactInfoVO);


  @ResponseStatus(value = HttpStatus.OK)
  @ApiOperation(value = "Get Contact Information", notes = "Returns a contact Information by Id")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Ok",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Success Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 400, message = "Bad Request",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Request Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 500, message = "Internal Server Error",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      })})
  @RequestMapping(value = "/{contactInfoId}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> getContactInfo(
    @ApiParam(value = "contactInfoId", required = true)
    @PathVariable(name = "contactInfoId", required = true) long contactInfoId);


  @ResponseStatus(value = HttpStatus.OK)
  @ApiOperation(value = "Get Contact Information", notes = "Returns a contact Information by email or/and phoneNumber")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Ok",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Success Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 400, message = "Bad Request",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Request Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 500, message = "Internal Server Error",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      })})
  @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> getContactInfo(
    @ApiParam(value = "email", required = false)
    @RequestParam(name = "email", required = false) String email,

    @ApiParam(value = "phoneNumber", required = false)
    @RequestParam(name = "phoneNumber", required = false) String phoneNumber);


  @ResponseStatus(value = HttpStatus.OK)
  @ApiOperation(value = "Get Contact Information", notes = "Returns a contact Information by location, where location can be either city or state")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Ok",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Success Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 400, message = "Bad Request",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Request Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 500, message = "Internal Server Error",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      })})
  @RequestMapping(value = "/location/{location}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<PagedResources<Resource<ContactInfoVO>>> getContactInfoByLocation(
    @ApiParam(value = "location", required = true)
    @PathVariable(name = "location", required = true) String location,

    @ApiParam(value = "page", required = false)
    @RequestParam(name = "page", required = false) Integer page,

    @ApiParam(value = "pageSize", required = false)
    @RequestParam(name = "pageSize", required = false) Integer pageSize);


  @ResponseStatus(value = HttpStatus.OK)
  @ApiOperation(value = "Deletes Contact Information", notes = "Deletes a contact information and returns the contact Information that was deleted")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Ok",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Success Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 400, message = "Bad Request",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Request Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      }),
    @ApiResponse(code = 500, message = "Internal Server Error",
      responseHeaders = {
        @ResponseHeader(name = "Message", description = "Error Message", response = String.class),
        @ResponseHeader(name = "ReasonCode", description = "Response Reason", response = String.class),
        @ResponseHeader(name = "RequestCorrelationId", description = "Initial Request GUID", response = String.class),
      })})
  @RequestMapping(value = "/{contactInfoId}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<Resource<ContactInfoVO>> deleteContactInfo(
    @ApiParam(value = "contactInfoId", required = true)
    @PathVariable(name = "contactInfoId", required = true) long contactInfoId);

}
