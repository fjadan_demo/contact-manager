## Contact Manager Service allows the following operations

    ● Create a contact record
    ● Retrieve a contact record
    ● Update a contact record
    ● Delete a contact record
    ● Search for a record by email or phone number
    ● Retrieve all records from the same state or city

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.


## To build the project

This is a Maven project and it is required to have Maven installed in order to build the project

To build the project, go to the root location of the project (where the pom.xml file is located) and issue the following at the command line:

       mvn package

The package goal will compile your Java code, run any tests, and finish by packaging the code up in a JAR file within the target directory

## To run the application

at the command line, after having built the project, execute the following

      java -jar target/contact-manager-0.0.1-SNAPSHOT.jar

This will start the application and the web container on the port 8080

## To check the health of the app

Once the application is up and running, open a new browser window and go to: localhost:8080/actuator/health

You should see something like this:

      {"status":"UP"}